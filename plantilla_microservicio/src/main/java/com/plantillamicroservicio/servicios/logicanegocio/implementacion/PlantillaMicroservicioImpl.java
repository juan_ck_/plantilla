package com.plantillamicroservicio.servicios.logicanegocio.implementacion;

import org.springframework.stereotype.Service;

import com.plantillamicroservicio.servicios.logicanegocio.IPlantillaMicroservicio;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
public class PlantillaMicroservicioImpl implements IPlantillaMicroservicio
{
  /**
   * Clase en la cual se debe poner toda la implementación de la logica de negocio definida en la interfaz
   */
}
