package com.plantillamicroservicio.servicios.controlador;

public class PlantillaMicroservicioController 
{
  /**
   * Aqui se define las operationes rest que se exponen. Ejemplo:
   *
   * @Autowired 
   * private IPlantillaMicroservicio plantillaMicroservicio;
   *
   * @PostMapping("/pruebaPlantilla")
   * @ApiResponses({ @ApiResponse(code = 200, message = "Se ejecuto satisfactoriamente."),
   * @ApiResponse(code = 400, message = "El cliente envio datos incorrectos."),
   * @ApiResponse(code = 500, message = "Error inesperado.") })
   * @ResponseBody
   * public List<Persona> consultarPersonas()
   * {
   *   plantillaMicroservicio.consultarPersonas();
   *     return plantillaMicroservicio.consultarPersonas().toString();
   * }
   **/
}
