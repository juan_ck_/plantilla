package com.plantillamicroservicio.servicios.repositorio;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.plantillamicroservicio.servicios.entidades.Entidad;

@Repository
public interface EntidadRepository extends CrudRepository<Entidad, Integer>
{

  /**
   * En esta interfaz vamos a definir las firmas que se van a utilizar con respecto a la base de datos, como consultas, por ejemplo:
   * 
   * @Query(value = "select * from everis.PERSONAS", nativeQuery = true)
   * public List<Persona> consultarPersonas(); 
   */
}
